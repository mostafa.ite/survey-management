@extends('layouts.app')
@section('content')
<div class="content">
    
    <div class="container">
        <div class="card-deck mb-3 text-center">
        
          <div class="card mb-4 box-shadow">
            <div class="card-header">
              <h4 class="my-0 font-weight-normal">Add Survey</h4>
            </div>
            <div class="card-body">
             
              <a href="{!! url('/survey/add') !!}" class="btn btn-lg btn-block btn-primary">Get started</a>
            </div>
          </div>
          <div class="card mb-4 box-shadow">
            <div class="card-header">
              <h4 class="my-0 font-weight-normal">Take Survey</h4>
            </div>
            <div class="card-body">
           
              <a href="{!! url('/survey/index') !!}" class="btn btn-lg btn-block btn-primary">Get started</a>
            </div>
          </div>
        </div>
</div>
@endsection
@section('scripts')
@parent

@endsection