<?php 

// echo "<pre>";
// print_r($submissions);
// echo "</pre>";

// exit;
?>

 @extends('layouts.app')
@section('css')

@stop

@section('content')
<div class="content">
<div >
    <h3>Submissions</h3>
</div>
    <div class="table-responsive">
        <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Date</th>
                    <th class="actions">Action</th>

                </tr>
            </thead>
            <tbody>
      
                @foreach($submissions as $submission)
                <tr>
                    <td>{{$submission->survey->title}}</td>
                    <td>{{$submission->created_at}}</td>
                 
                <td class="actions"> <a href="{{url('/survey/result' ,['submission_id' => $submission->id ])}}">View </a></td>
                </tr>
              
                @endforeach
            </tbody>
        </table>
           {{-- Pagination --}}
           <div class="d-flex justify-content-center">
            {!! $submissions->links() !!}
        </div>
    </div><!-- /.table-responsive -->
</div>
@endsection

@section('scripts')


@endsection