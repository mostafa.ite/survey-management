@extends('layouts.app')
@section('css')
    <style>
        .info {
    color: #1e9ff2!important;
}
    </style>
@endsection

@section('content')
    
<div class="content">
    <div class="wrapper fadeInDown">
        <h1>{{$survey->title}}</h1>
    </div>
    <div class="question">
     <form action="">
        <div class="card" >
            <div class="card-content">
               
                <div class="card-body">                
                    
                    <p class="card-text q" data-id={{$survey->questions->first()->id}}>  {{$survey->questions->first()->question}}   </p>
                   
                   
                </div>
                <div class="card-footer text-center fset">
                    <a href="#" class="btn btn-outline-success next" data-answer='1'>Yes</a>
                    <a href="#" class="btn btn-outline-danger next" data-answer='0'>No</a>
                </div>
            </div>
        </div>
        <div class="card ncard">            
            <textarea cols="50" rows="3" name="note" id="text_id" class="form-control note" style="resize:vertical;display:none"></textarea>
            <button class="btn btn-primary show_button">Add Note</button>
           
        </div>

     </form>
     
    </div>
</div>
@endsection
@section('scripts')

<script>$(".show_button").click(function(e){
    e.preventDefault();    
    $("#text_id").toggle()}
    )
</script>

<script>
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    let answers = [];
   
    	$(".next").click(function()  {
            $(".card").fadeOut();         
            
            // console.log('click');
            
               let answerIndex = $(this).data("answer") ;
            //    console.log(answerIndex);
					answers.push({
						question_id: $(".q").attr("data-id"),
						answer: answerIndex == 1 ? "y" : "n",
						notes: $(".note").val()
                    });
                    $("#text_id").html('');
                    // console.log(answers);
                    $.ajax({
                        type: "GET",
                        url:"/nextQuestion/"+$(".q").attr("data-id")+"/"+answerIndex,
                        
                     
                        success: function(response) {
                 
                            if(Object.keys(response).length != 0 ) {
                                console.log('responce' ,response);
                                response = JSON.parse(response);
								$(".q").attr('data-id', response.id);
								$(".q").html(response.question);
                                $(".note").val("");
                                $(".card").fadeIn();
                            }else{
                                $(".card").fadeIn();
                                console.log('empty responce');
                              
                                $(".fset").html(
					                 '<button type="button" class="btn btn-primary btn-lg" style="margin: 10px;" id="submit">Submit</button>'
                                 );
                                $(".ncard").hide();
                                $("#submit").click(function()  {
											$.ajax({
												type: "POST",
												url: "/submit-answer",
												dataType: "json",
												data: {'survey_id':{{$survey->id}} ,answers:answers },
												success: function(response) {
													console.log("resp", response);
												}
                                            });
                                            
										
                                });
                            }
                            
                            
                        },

                    })
				
				});
</script>
@endsection