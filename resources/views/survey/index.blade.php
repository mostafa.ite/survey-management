@extends('layouts.app')
@section('css')

@stop

@section('content')
<div class="content">
    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <h5>Surveys List</h5>
    </div>
    <div class="table-responsive">
        <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>submissions</th>
                    <th class="actions">Action</th>
                </tr>
            </thead>
            <tbody>
      
                @foreach($surveys as $survey)
                <tr>
                    <td>{{$survey->title}}</td>
                    <td class="actions"> <a href="{!! url('/survey/submissions',['id'=>$survey->id]) !!}">View submissions({{$survey->submissions->count()}})</a></td></td>
                    <td class="actions"> <a href="{!! url('/survey/take',['id'=>$survey->id]) !!}">Take survey</a></td>
                </tr>
              
                @endforeach
            </tbody>
        </table>
           {{-- Pagination --}}
           <div class="d-flex justify-content-center">
            {!! $surveys->links() !!}
        </div>
    </div><!-- /.table-responsive -->
</div>
@endsection

@section('scripts')


@endsection