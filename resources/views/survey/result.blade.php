
<?php 

// echo "<pre>";
// print_r($result);
// echo "</pre>";
// exit();?>
@extends('layouts.app')
@section('css')

@stop



@section('content')
<div class="content">
    {{-- @foreach($result as $survey)
    {{$survey->question}}
    @endforeach --}}
 <div class="row">
    <div class="col-md-12">
        <div class="card" >
            
            <div class="card-body">
                <h4 class="card-title text-center">{{$submission->survey->title}}</h4>
            <h6 class=" text-center"> submit date: {{$submission->created_at}}</h6>
                <hr>
                <?php 
                 $answers = $submission->answers->sortBy('id');                
                ?>
                @foreach($answers as $answer)
                    
                <div class="form-group">
                    <label for=""></label>
                    <h5 style="color: #4E5E6A;"> {{$answer->question->question}}</h5>
                </div>
               
                <div class="question-answer <?=$answer->answer == 'y'? 'text-success': 'text-danger'  ?>"  >
                    Answer : <?= $answer->answer == 'y' ? 'Yes' : 'No'?>
                </div>

                @endforeach
            </div>
        </div>
    </div>
 </div>
</div>
@endsection

@section('scripts')


@endsection