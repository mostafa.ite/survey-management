@extends('layouts.app')
@section('css')
<!-- <link rel="stylesheet" href="{{ asset('css/jquery.hortree.css') }}"> -->
@stop

@section('content')
<section>
   
    <div id="questions" style="z-index: -1;"></div>


</section>
<section class="content" >   

<br>

    <!-- <form> -->
        <div class="form-group">
            <label for="title">Survey Title :</label>
            <input type="text" class="form-control" id="title" name="title" placeholder="survey title ...">
        </div>
        <div class="form-group">
            <label for="email">Email address :</label>
            <input type="email" class="form-control" id="creator_email" name="creator_email" placeholder="name@example.com">
        </div>

        <div class="form-group">
            <button type="submit" id="submit" class="btn btn-primary">Submit</button>
        </div>
    <!-- </form> -->

    <br>
    <div>
    <div class="card qform" style="
    width: 80%;
    padding: 20px;   
	display: none;
	position:absolute;
	left:0;
	right:0;
	margin-left:auto;
	margin-right:auto;
	top: 50px;
	background-color:#f8fafc ;
	z-index: 100;
	">

            <h3 class="card-header info-color white-text text-center py-4" style="    background-color: #fff;
            border: 1px solid #e9ecef;">
                <strong>Enter Question Info</strong>
            </h3> <br>

            <!--Card content-->
            <div class="card-body px-lg-5">

                <!-- Form -->
                <form class="text-center" style="color: #757575;">

                    <!-- Name -->
                    <div class="md-form mt-3">
                        <label for="qtext">Question</label>
                        <textarea class="form-control" id="qtext" cols="150" rows="3" placeholder="Enter a question..." autofocus></textarea>
                    </div> <br>
                    <div class="md-form mt-3">
                        <label for="qtext" class="text-success">Next question on "YES"</label>
                        <textarea class="form-control" cols="150" rows="3" id="yq" placeholder="Enter a &quot;YES&quot; question..."></textarea>
                    </div> <br>
                    <div class="md-form mt-3">
                        <label for="qtext" class="text-danger">Next question on "NO"</label>
                        <textarea class="form-control" cols="150" rows="3" id="nq" placeholder="Enter a &quot;NO&quot; question..."></textarea>
                    </div>

                    <br>
                    <button class="form-control btn btn-outline-primary btn-lg align-middle" style="width: 10%; vertical-align: middle;" type="button" id="save-q">Save</button>

                    <button class="form-control btn btn btn-outline-danger btn-lg align-middle" id="qform-close" style="width: 10%; vertical-align: middle;" type="button">Cancel</button>

                </form>
                <!-- Form -->

            </div>

        </div>
        <!-- Material form subscription -->
        <!-- <h1>Add</h1> -->
       
    </div>
</section>

@endsection

@section('scripts')
<script src="{{ asset('js/survey_validation.js') }}"></script>
@endsection