<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/jquery.hortree.css') }}">

    @yield('css')

</head>

<body >
   
      <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
        <h5 class="my-0 mr-md-auto font-weight-normal">Anonymous Survey System</h5>
        <nav class="my-2 my-md-0 mr-md-3">
          <a class="p-2 text-dark" href="{!! url('/survey/add') !!}">Add</a>
          <a class="p-2 text-dark" href="{!! url('/survey/index') !!}">List</a>
          <a class="p-2 text-dark" href="#">charts</a>
         
        </nav>
    
      </div>
 
        <div class="container px-3 py-3 pt-md-5 pb-md-4 mx-auto">

            @yield("content")
        </div>
  
    <!-- Scripts -->

    
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> 
    
    {{-- <script src="{{ asset('js/app.js') }}" defer></script>  --}}
    
    <script src="{{ asset('js/jquery.line.js') }}"></script>
    <script src="{{ asset('js/jquery.hortree.js') }}"></script>

    <!-- <script src="{{ asset('js/survey_validation.js') }}"></script> -->
    @yield('scripts')
</body>

</html>