<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Submission extends Model
{
    public $table = 'submissions';

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    protected $fillable = [
        'survey_id',
        'session_id',
    ];

    public function answers(){
        return $this->hasMany(Answer::class);
    }
    public function survey(){
        return $this->belongsTo(Survey::class);
    }
}
