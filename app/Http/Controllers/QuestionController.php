<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Question;
use Illuminate\Http\Request;

class QuestionController extends Controller
{

    public function nextQuestion($id, $answerIndex)
    {
        // return json_encode($answerIndex .''.$id);
        // $this->autoRender = false;
        // $options = array('conditions' => array(
        //     'Question.parent_q_id' => $id,
        //     'Question.on_yes_no' => $answerIndex,
        // ));
        // DB::enableQueryLog();
        $nextQuestion = Question::where('parent_id', $id)
        ->where('on_yes_no',$answerIndex == 1 ? 'y':'n')
        ->get()->first();;
        // $query = DB::getQueryLog();
        // return json_encode($query);
        //   $nextQuestion = Question::find($id)->YesChildren();
    //     // $q = $this->Question->find('first', $options);
        if (isset($nextQuestion) && !empty($nextQuestion)) {
            
            return json_encode($nextQuestion);
        }
    }
}
