<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Submission;
use App\Survey;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AnswerController extends Controller
{
    //


    public function store(Request $request)
    {
        // print_r($request->all());
        // exit;
        // $request->validate([
        //     'question_id'           => 'required',
        //     'answer'   => 'required',

        // ]);
        DB::beginTransaction();

        try {
            $submission = Submission::create(['survey_id' => $request->survey_id, 'session_id' => session()->getid()]);

            foreach ($request->answers as $answer) {
                $CreatedAnswer =  Answer::create($answer);
                $CreatedAnswer->submission_id = $submission->id;
                $CreatedAnswer->save();
            }

            DB::commit();
            // all good
            // $url = route('/survey/result', ['id' => $submission->id]);
            return response()->json(['success' => '/survey/result/'.$submission->id]);
        } catch (\Exception $e) {
            DB::rollback();
            // something went wrong
            return response()->json(['error' => 'answer wasnot submitted!']);
        }
        // $answer  =  Answer::create($request->all());




        // $answer  =  Answer::create([
        //     'question_id' => $request->question_id,
        //     'answer'=>$request->answer,
        //     'notes'=>$request->notes,
        //     // 'session_id'=>session()->getid()
        // ]);
        // return response()->json(['success' => 'answer is successfully submitted!']);
    }

    public function result($survey_id)
    {
        //check if redirected form submit
        //   $answers= Survey::with(questio)
        DB::enableQueryLog();
        $result = DB::table('surveys')
            ->leftJoin('questions', 'surveys.id', '=', 'questions.survey_id')
            ->leftjoin('answers', 'questions.id', '=', 'answers.question_id')
            ->where('surveys.id', '=', $survey_id)
            ->where('answers.session_id', '=', session()->getid())
            ->get();
        $query = DB::getQueryLog();
        // ->where('answers.session_id', '=', session()->getid())        
        return json_encode($query);
        //else

        // return view('survey.index', compact('surveys'));
    }
}
