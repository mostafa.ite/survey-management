<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Survey;
use App\Question;
use App\Submission;
use Illuminate\Support\Facades\DB;
use Session;

class SurveyController extends Controller
{


    public function index()
    {
        // $surveys = Survey::all();
        $surveys = Survey::paginate(5);
        return view('survey.index', compact('surveys'));
    }

    //create survey
    /**
     * @param request is the request object
     */
    public function store(Request $request)
    {
        $request->validate([
            'title'           => 'required',
            'creator_email'   => 'required|email',

        ]);


        //db transaction
        DB::beginTransaction();

        try {
            $survey  =  Survey::create($request->all());
            // print_r($survey);
            $question = $request->questions;
          
            if (!empty($survey)) {
                $first_question = Question::create(['survey_id' => $survey->id, 'question' => $question['question']]);
                if (isset($question['children'][0])) {
                    $this->saveChildQs($question['children'][0], $first_question);
                }
                if (isset($question['children'][1])) {
                    $this->saveChildQs($question['children'][1], $first_question);
                }

                DB::commit();
                return response()->json(['success' => 'Form is successfully submitted!']);
            }

            // all good
        } catch (\Exception $e) {
            DB::rollback();
            // something went wrong
            return response()->json(['error' => 'error while submiting !']);
        }
    }

    
    /**
     * @param id is survey_id
     * redirect to take survey page
     * get
     */
    public function take($id)
    {
        // $survey =Survey::find($id);
        $survey = Survey::findOrFail($id);
        return view('survey.take', compact('survey'));
    }

    /**
     * @param q is question , 
     * @param parent in questioon
     * recursive function to save the question yes no children
     */
    private function saveChildQs($q, $parent)
    {
        $child_question = Question::create([
            'survey_id' => $parent->survey_id,
            'question' => $q['question'],
            'on_yes_no' => $q['type'],
            'parent_id' => $parent->id
        ]);

        if (isset($q['children'][0])) {
            $this->saveChildQs($q['children'][0], $child_question);
        }
        if (isset($q['children'][1])) {
            $this->saveChildQs($q['children'][1], $child_question);
        }
    }

    /**
     * @param id is survey id      *
     *  function to get the submissions of a survey
     */
    public function submissions($id)
    { 
        DB::enableQueryLog();
        $submissions = Survey::findOrfail($id)->submissions()->orderBy('id' ,'Desc')->paginate(5);
        // $results = DB::table('answers')
        //     //// ->leftJoin('questions', 'surveys.id', '=', 'questions.survey_id')
        //     //// ->select('answers.*','questions.question', 'surveys.title')
        //     ->select(DB::raw('answers.* ,questions.question ,questions.survey_id ,surveys.title'))
        //     ->leftjoin('questions', 'questions.id', '=', 'answers.question_id')
        //     ->leftJoin('surveys', 'surveys.id', '=', 'questions.survey_id')
        //     ->where('questions.survey_id', '=', $id)
        //     ->where('questions.parent_id', '=', Null)
        //     ->orderBy('answers.created_at')
        //     ->paginate(6);
        //// ->get();
        // dd(DB::getQueryLog());
        return view('survey.submissions', compact('submissions'));
    }

    public function result($survey_id)
    {
        //check if redirected form submit
        // if($submit){
        $result = DB::table('surveys')
            ->leftJoin('questions', 'surveys.id', '=', 'questions.survey_id')
            ->leftjoin('answers', 'questions.id', '=', 'answers.question_id')
            ->where('surveys.id', '=', $survey_id)
            ->where('answers.session_id', '=', session()->getid())
            ->get();
        // }else{

        // }
        //   $answers= Survey::with(questio)

        // ->where('answers.session_id', '=', session()->getid())        
        // return json_encode($query);
        //else

        return view('survey.result', compact('surveys'));
    }


    public function viewResult($survey_id, $session_id = null)
    {
        //check if redirected form submit
        if ($session_id == null) {
            $session_id = session()->getid();
        }
        $survey = Survey::findORFail($survey_id);
        // print_r($survey);exit;
        $result = DB::table('answers')
            // ->leftJoin('questions', 'surveys.id', '=', 'questions.survey_id')
            ->leftjoin('questions', 'questions.id', '=', 'answers.question_id')
            ->where('questions.survey_id', '=', $survey_id)
            ->where('answers.session_id', '=', $session_id)
            ->orderBy('answers.id')
            ->get();
        $survey['questions'] = $result;

        return view('survey.result', compact('survey'));
    }
    /**
     * @param submission_id is submission record id      *
     *  function to get the results a submission of a survey
     */
    public function viewResult2($submission_id){
        $submission = Submission::findOrfail($submission_id);
        // $answers = $submission->answers->sortBy('id');
        // $result['title'] = $submission->survey->title;
        // $result['answers'] = $answers;
        return view('survey.result', compact('submission'));
        
    }
    // public function viewResult3($submission_id){
    //     $submission = Submission::where('id',$submission_id)->;
    //     // $answers = $submission->answers->sortBy('id');
    //     // $result['title'] = $submission->survey->title;
    //     // $result['answers'] = $answers;
    //     return view('survey.result', compact('submission'));
        
    // }
}
