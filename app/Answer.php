<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    public $table = 'answers';

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    protected $fillable = [
        'answer',
        'question_id',
        'notes',
        'submission_id'
    ];

    public function question(){
        return $this->belongsTo(Question::class, 'question_id');
    }
}
