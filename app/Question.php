<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public $table = 'questions';

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    protected $fillable = [
        'question',
        'on_yes_no',
        'survey_id',
        'parent_id',
    ];

    public function survey()
    {
        return $this->belongsTo(Survey::class);
    }
   
    public function parent()
    {
        return $this->belongsTo(Question::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(Question::class, 'parent_id');
    }

    public function YesChildren()
    {
        return $this->hasMany(Question::class, 'parent_id')->where('on_yes_no','=', 'y');
    }

    public function NoChildren()
    {
        return $this->hasMany(Question::class, 'parent_id')->where('on_yes_no','=', 'n');
    }
}
