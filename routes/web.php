<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/survey/add', function () { 
    // example will be the subdirectory under views and examplebook will be the blade view 'examplebook.blade.php'
   return view('survey.add'); 
});
Route::get('/survey/index', 'SurveyController@index');
Route::get('/survey/take/{id}', 'SurveyController@take');
Route::get('/survey/submissions/{id}', 'SurveyController@submissions');
Route::get('/survey/result/{submission_id}', 'SurveyController@viewResult2');
// Route::get('/survey/take',  function () { 
//     // example will be the subdirectory under views and examplebook will be the blade view 'examplebook.blade.php'
//    return view('survey.take'); 
// });

Route::post('survey-form', 'SurveyController@store');

Route::get('nextQuestion/{id}/{answer}', 'QuestionController@nextQuestion');


Route::post('submit-answer','AnswerController@store');
// Route::get('viewAnswers/{id}','AnswerController@result');
